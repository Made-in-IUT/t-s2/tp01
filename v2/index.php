<h1>Anniversaires du jour</h1>

<form style="padding:1rem; border: 1px solid #a0aec0;" action="/tp1/v2/index.php" method="GET">
  <label style="margin-right: 2rem">
    Nom
    <input type="text" name="name">
  </label>

  <label style="margin-right: 2rem">
    Date de naissance
    <input type="text" name="birthday">
  </label>

  <label>
    Afficher l'age
    <input type="checkbox" name="showAge" checked>
  </label>

  <br />

  <button style="margin-top:1.5rem" type="submit">Ajouter</button>
</form>

<?php
// Form
if (isset($_GET['name']) && isset($_GET['birthday'])) {
  $name = strtolower($_GET['name']);
  $birthday = $_GET['birthday'];
  $showAge = isset($_GET['showAge']) ? $_GET['showAge'] : 'off';

  if ($name !== '' && $birthday !== '') {
    file_put_contents('etu.csv', "\n$name;$birthday;$showAge", FILE_APPEND);

    copy('../assets/durandm.jpg', "../assets/$name.jpg");
  }
}

// Core
$file = fopen("etu.csv", "r");
$date = getdate();

while (!feof($file)) {
  $student = explode(';', fgetcsv($file)[0]);
  $birthday = explode('/', $student[1]);

  if ($birthday[1] == $date['mon'] && $birthday[0] == $date['mday']) {
    echo "<img src='./assets/$student[0].jpg'/>";
    echo "<p><span style='font-weight: 600'>Nom</span>: $student[0]</p>";
    echo "<p><span style='font-weight: 600'>Date de naissance</span>: $student[1]</p>";

    if (isset($student[2]) && $student[2]) {
      $age = $date['year'] - $birthday[2];

      echo "<p><span style='font-weight: 600'>Age</span>: $age</p>";
    }
  }
}

fclose($file);
